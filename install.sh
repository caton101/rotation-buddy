#!/bin/bash

echo "Getting sudo access..."
sudo -v

echo "Installing required packages..."
sudo apt install python3 python3-pip gir1.2-appindicator3-0.1

echo "Installing required Python packages..."
sudo pip3 install pystray

echo "Creating program directory..."
sudo mkdir /usr/local/share/rotationBuddy

echo "Copying program files..."
sudo cp ./src/main.py       /usr/local/share/rotationBuddy/main.py
sudo cp ./src/rotation.py   /usr/local/share/rotationBuddy/rotation.py
sudo cp ./src/rotationBuddy /usr/local/share/rotationBuddy/rotationBuddy

echo "Changing file permissions..."
sudo chmod +x /usr/local/share/rotationBuddy/rotationBuddy

echo "Adding program to PATH..."
sudo ln -s /usr/local/share/rotationBuddy/rotationBuddy /usr/local/bin/rotation-buddy

echo "Adding program to menu..."
sudo cp ./src/rotationBuddy.desktop /usr/share/applications/rotationBuddy.desktop

echo "Adding program to autostart..."
sudo cp ./src/rotationBuddy.desktop $HOME/.config/autostart/rotationBuddy.desktop

echo "Installation is complete! Reboot to start Rotation Buddy"

exit
