import subprocess
import sys

# NOTE: rotation commands came from https://www.raspberrypi.org/forums/viewtopic.php?t=264107


def rotateNormal(fake=False, isVerbose=True):
    if isVerbose:
        print("Rotating to NORMAL...")
    if not fake:
        #         xrandr    --output    DSI-1    --rotate    normal
        __run__(["xrandr", "--output", "DSI-1", "--rotate", "normal"])
        #         xinput    --set-prop   "FT5406 memory based driver"  "Coordinate Transformation Matrix"   0    0    0    0    0    0    0    0    0
        __run__(["xinput", "--set-prop", "FT5406 memory based driver", "Coordinate Transformation Matrix", "0", "0", "0", "0", "0", "0", "0", "0", "0"])
    if isVerbose:
        print("Rotated to NORMAL.")


def rotateLeft(fake=False, isVerbose=True):
    if isVerbose:
        print("Rotating to LEFT...")
    if not fake:
        #         xrandr    --output    DSI-1    --rotate    left
        __run__(["xrandr", "--output", "DSI-1", "--rotate", "left"])
        #         xinput    --set-prop   "FT5406 memory based driver"  "Coordinate Transformation Matrix"   0    -1    1    1    0    0    0    0    1
        __run__(["xinput", "--set-prop", "FT5406 memory based driver", "Coordinate Transformation Matrix", "0", "-1", "1", "1", "0", "0", "0", "0", "1"])
    if isVerbose:
        print("Rotated to LEFT.")


def rotateRight(fake=False, isVerbose=True):
    if isVerbose:
        print("Rotating to RIGHT...")
    if not fake:
        #         xrandr    --output    DSI-1    --rotate    right
        __run__(["xrandr", "--output", "DSI-1", "--rotate", "right"])
        #         xinput    --set-prop   "FT5406 memory based driver"  "Coordinate Transformation Matrix"   0    1    0    -1    0    1    0    0    1
        __run__(["xinput", "--set-prop", "FT5406 memory based driver", "Coordinate Transformation Matrix", "0", "1", "0", "-1", "0", "1", "0", "0", "1"])
    if isVerbose:
        print("Rotated to RIGHT.")


def rotateInverted(fake=False, isVerbose=True):
    if isVerbose:
        print("Rotating to INVERTED...")
    if not fake:
        #         xrandr    --output    DSI-1    --rotate    inverted
        __run__(["xrandr", "--output", "DSI-1", "--rotate", "inverted"])
        #         xinput    --set-prop   "FT5406 memory based driver"  "Coordinate Transformation Matrix"   -1    0    1    0    -1    1    0    0    1
        __run__(["xinput", "--set-prop", "FT5406 memory based driver", "Coordinate Transformation Matrix", "-1", "0", "1", "0", "-1", "1", "0", "0", "1"])
    if isVerbose:
        print("Rotated to INVERTED.")


def __run__(command):
    process = subprocess.run(
        command, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE)
    if process.returncode != 0:
        raise ChildProcessError(
            "The subprocess command failed.\nReason: %s" % process.stderr.decode())
