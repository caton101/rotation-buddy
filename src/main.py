# this code is modified from https://github.com/moses-palmer/pystray/blob/master/docs/usage.rst

from pystray import Icon as icon, Menu as menu, MenuItem as item
from PIL import Image, ImageDraw
import subprocess
import rotation


ROTATION_NORMAL = 0
ROTATION_LEFT = 1
ROTATION_RIGHT = 2
ROTATION_INVERTED = 3
OPTION_FAKE = True
OPTION_REAL = False
OPTION_LOUD = True
OPTION_QUIET = False

callMode = OPTION_REAL
callOutput = OPTION_QUIET
bufferSize = 2 ** 12  # 4 KB
width  = 32
height = 32
color1 = (255,   0,   0)
color2 = (  0,   0, 255)


def create_image():
    # Generate an image and draw a pattern
    image = Image.new('RGB', (width, height), color1)
    dc = ImageDraw.Draw(image)
    dc.rectangle(
        (width // 2, 0, width, height // 2),
        fill=color2
    )
    dc.rectangle(
        (0, height // 2, width // 2, height),
        fill=color2
    )

    return image


state = ROTATION_NORMAL


def updateRotation():
    global state, callMode, callOutput
    if state == ROTATION_NORMAL:
        # change to NORMAL
        rotation.rotateNormal(callMode, callOutput)
    elif state == ROTATION_LEFT:
        # Change to LEFT
        rotation.rotateLeft(callMode, callOutput)
    elif state == ROTATION_RIGHT:
        # Change to RIGHT
        rotation.rotateRight(callMode, callOutput)
    elif state == ROTATION_INVERTED:
        # Change to INVERTED
        rotation.rotateInverted(callMode, callOutput)
    else:
        print("Invalid state.")


def set_state(v):
    def inner(icon, item):
        global state
        state = v
        updateRotation()
    return inner


def get_state(v):
    def inner(item):
        return state == v
    return inner


options = [
    item('NORMAL',   set_state(ROTATION_NORMAL),   checked=get_state(ROTATION_NORMAL),   radio=True),
    item('LEFT',     set_state(ROTATION_LEFT),     checked=get_state(ROTATION_LEFT),     radio=True),
    item('RIGHT',    set_state(ROTATION_RIGHT),    checked=get_state(ROTATION_RIGHT),    radio=True),
    item('INVERTED', set_state(ROTATION_INVERTED), checked=get_state(ROTATION_INVERTED), radio=True)
]


i = icon('test', create_image(), menu=options)
i.run()
